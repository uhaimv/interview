package com.example.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;

/**
 * Created by HaiMV on 7/9/2016.
 */
@Configuration
public class WebConfig {

    @Bean
    public Filter gzipFilter(){
        return new GZIPFilter();
    }

}
