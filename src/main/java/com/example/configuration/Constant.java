package com.example.configuration;

/**
 * Created by HaiMV on 7/9/2016.
 */
public final class Constant {

    private Constant(){

    }

    public static final String SAY_HELLO = "Hello guest!";
    public static final String BASE_PACKAGE = "com.example.*";
    public static final String HOME_VIEW = "home";
    public static final String LOGIN_SUCCESS = "loginsucess";

}
