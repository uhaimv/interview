package com.example.controller;

import com.example.configuration.Constant;
import com.example.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by HaiMV on 7/9/2016.
 */
@Controller
public class HelloController {

    @Autowired
    private HelloService helloService;

    @RequestMapping(value = "/sayhello",method = RequestMethod.GET)
    @ResponseBody
    public String sayHello(){
        //call service and return data
        return helloService.sayHello();
    }

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String home(){
        //review home page
        return Constant.HOME_VIEW;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@RequestParam(value = "username") String username){
        //return a successful view
        return Constant.LOGIN_SUCCESS;
    }
}
