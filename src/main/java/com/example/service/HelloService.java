package com.example.service;

import com.example.configuration.Constant;
import org.springframework.stereotype.Service;

/**
 * Created by HaiMV on 7/9/2016.
 */
@Service
public class HelloService {

    public String sayHello(){
        //return data for client
        return Constant.SAY_HELLO;
    }

}
